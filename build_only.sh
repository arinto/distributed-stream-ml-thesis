#!/bin/sh

MAIN_FILE="thesis-main"

echo "Creating the pdf files!"
pdflatex -synctex=1 -interaction=nonstopmode $MAIN_FILE.tex
bibtex $MAIN_FILE.aux
pdflatex -synctex=1 -interaction=nonstopmode $MAIN_FILE.tex
pdflatex -synctex=1 -interaction=nonstopmode $MAIN_FILE.tex
