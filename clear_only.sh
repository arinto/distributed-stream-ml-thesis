#!/bin/sh

rm *.log
rm *.pdf
rm *.aux
rm *.bbl
rm *.blg
rm *.lof
rm *.lot
rm *.synctex.gz
rm *.toc
