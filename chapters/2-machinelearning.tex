%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               Machine Learning
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Machine Learning}
\label{chapter:machine_learning}
Machine learning (ML) is a branch of artificial intelligence which focuses on development of systems that can learn and adapt based on the data. It involves application of statistics to perform data mining tasks. In general, ML consists of learning phase to build the model and evaluation phase to measure the model performance based on several metrics.
To speed-up the analysis using ML, some frameworks are developed. One prominent example is WEKA framework~\cite{hall_weka_2009}. WEKA contains comprehensive collection of algorithms for ML and tools for data preprocessing. Furthermore,  several graphical user interfaces are also contained in WEKA to improve the underlying functionalities. It allows researchers and data mining practitioners to perform rapid prototyping of existing algorithm for new dataset in order to solve their problems. The important characteristics of  these frameworks and algorithms are: they run sequentially in single machine, all data need to be fit in the memory and they require multiple pass into the dataset during learning phase. In this report, we will refer these early generation of ML framework as \textit{conventional frameworks}.

\section{Distributed Machine Learning}
\label{sec:distributedML}
The abundance of data and the need to efficiently process them have triggered research and development of the conventional frameworks. As a result, distributed and parallel ML frameworks were developed.

Mahout~\footnote{https://mahout.apache.org/} is a distributed ML framework on top Hadoop. It contains common algorithms for classification, clustering, recommendation, and pattern mining. The main goal of Mahout is to provide scalability when analyzing large amount of data, i.e. more processing power can be easily added to cope with additional data.

Distributed GraphLab~\cite{low_distributed_2012} is a distributed implementation of GraphLab~\cite{low_graphlab:_2010} abstraction. This abstraction is built using the \textit{shared-memory} model. It targets ML algorithms that utilize many \textit{asynchronous, dynamic, graph-parallel} computation such as alternating least squares for movie recommendation, video co-segmentation and named-entity-recognition. Notable features of Distributed GraphLab platform are two implementations of GraphLab engines: a chromatic engine for partial synchronous with the assumption of graph-coloring existence, and a locking engine for fully asynchronous case. This platform also includes fault tolerance by introducing two snapshot algorithms: a synchronous algorithm, and a fully asynchronous algorithm based on Chandy-Lamport snapshots. Furthermore, distributed-data-graph format is also included in this platform.

Google developed Pregel~\cite{malewicz_pregel:_2010}, that serves the same purpose as Graphlab. Instead of utilizing shared-memory model, Pregel employs message-passing model. It is materialized as open source implementation in the form of Apache Giraph~\footnote{http://giraph.apache.org/}.%ML algorithms for graph mining are currently being implemented on top of Giraph. [ref: check with Maria]

MLBase~\cite{kraska_mlbase:_2013} aims to ease non-experts access to ML. It simplifies ML utilization by transforming user-friendly ML declarative plan into refined learning plan i.e. MLBase returns the current best solution for the plan, and performs optimization in the background. To support this use case, MLBase masks the underlying complexity of distributed computation by providing a set of high-level operators for implement a scalable ML algorithm. MLBase's distributed execution engine is implemented on top of Spark~\cite{zaharia_resilient_2012} and Mesos~\cite{hindman_mesos:_2011}, and it is part of Berkeley Data Analytics Stack~\footnote{https://amplab.cs.berkeley.edu/bdas/}.

There are also some works that try to utilize MapReduce as for distributed ML implementation. One example is in ~\cite{gillick_mapreduce:_2006}, in which the authors analyzed the taxonomy of ML learning algorithms and classified them based on the procedural character. They divided the algorithms into three categories: single-pass learning, iterative, and query-based learning. Furthermore, they identified issues related to MapReduce implementation for each category. The main issue related to MapReduce implementation was the high cost of distributing data into Hadoop cluster. Some recommendations in tailoring MapReduce for ML were also proposed, they are: to provide map tasks with a shared space on a name node, to enable MapReduce input reader tasks to concurrently process multiple parallel files, and proper static type checking in Hadoop. This work shows that distributed and scalable ML is feasible, but not all ML algorithms can be efficiently and effectively implemented in distributed setting.

\section{Streaming Machine Learning}
\label{sec:streamingML}
Another development of ML is in processing continuous supply of data. In the conventional ML, an induced model is not designed to be updated with the arrival of new data. The training needs to be performed again using the existing data plus the newly arrived data and it is very time-consuming. Furthermore, it is not feasible to store all the incoming data in the memory since the arriving data is unbounded. These characteristics are often undesirable since they decrease the speed of the conventional ML algorithms which leads to inability in processing continuous supply of data.

Streaming ML paradigm has emerged to address the aforementioned continuous data supply challenge. This paradigm is characterized by:
\begin{enumerate}
	\item High data rate and volume, such as  social media data, transactions logs in ATM or credit card operations and call logs in telecommunication company.
 	\item Unbounded, which means the data always arrive with the infinite amount. This implies, multiple passes of analysis can not be performed due to inability of memory or disk to hold infinite amount of the data. The streaming ML algorithm needs to only analyze the data once and within small bounded time. 
\end{enumerate}

Another aspect of streaming ML is change detection. This aspect concerns the ability of a ML algorithm in detecting changes in incoming data characteristics. Once, the changes are detected, the induced model needs to be updated accordingly. ML algorithms for streaming are often called \textit{online algorithms}.

One of the existing streaming ML frameworks is Massive Online Analysis (MOA)~\cite{bifet_moa:_2010}. MOA consists of well-known online algorithms for streaming classification, clustering, and change detection mechanisms. Several variants of prequential, hold-out procedure classification evaluation and clustering evaluation are also included in MOA. It also provides extension points for ML developers to implement their own streaming source generators, learning algorithms and evaluation methods. Visualization tools for clustering are implemented in MOA. However, MOA is designed to work sequentially in single machine which implies limited scalability when facing with ever increasing data stream.

Vowpal Wabbit~\footnote{https://github.com/JohnLangford/vowpal\_wabbit/wiki} is another example of streaming ML framework. It is based on perceptron model and optimized for text data input.  Several online algorithms are contained in Vowpal Wabbit such as stochastic gradient descent, truncated gradient for sparse online learning, and adaptive subgradient methods.

Debellor~\cite{wojnarski_debellor:_2008} is an open source stream-oriented platform for data mining. It utilizes component-based architecture, i.e. an online algorithm is implemented in term of components, and each component communicates to another component via streamed data. This architecture allows ML developers to easily extend the framework with their own implementation of the components. It also claims to be "scalable" in term of the input size i.e. Debellor is able to cope with the increase in input size which could not be handled by corresponding batch variant. This means Debellor only supports vertical scalability and it does not support horizontal scalability.

Another direction in streaming ML is to utilize existing stream processing engine (SPE) in the implementation of online algorithms. SPE is a computation engine that specialized to process high volume of incoming data stream with low latency. Stonebraker~\cite{stonebraker_8_2005} defines SPE as a system that apply SQL-style on the data stream on the fly, but this system is not required to store the data. There are open source SPEs such as Aurora~\cite{abadi_aurora:_2003}. And there are also commercial SPEs such as IBM InfoSphere Streams~\footnote{http://www.ibm.com/software/data/infosphere/streams}, Microsoft StreamInsight~\footnote{http://www.microsoft.com/en-us/sqlserver/solutions-technologies/business-intelligence/streaming-data.aspx}, and StreamBase~\footnote{http://www.streambase.com}. SPE is evolving  and the state-of-the art SPE utilizes MapReduce programming model instead of SQL-style programming model to process incoming data stream. Prominent examples of the state-of-the-art SPEs are S4~\cite{neumeyer_s4:_2010} and Storm~\footnote{http://storm-project.net/}.

\textit{storm-pattern} project~\footnote{https://github.com/quintona/storm\-pattern} is an effort to adapt existing ML model into Storm using Trident abstraction. In the current version, the model is trained using batch mode, i.e. it only allows online scoring, but not online training. Moving forward, online Rprop is going to be added into storm-pattern so that it allows online training and scoring.

\section{Distributed Streaming Machine Learning}
\label{sec:distributed_stream_ml}
Existing streaming machine learning frameworks, such as MOA, are unable to scale when handling very high incoming data rate. They are designed to execute sequentially in a single machine. In order to solve this scalability issue, there are several possible solutions such as to make the streaming machine learning framework distributed and to make the online algorithms run in parallel.

Jubatus\footnote{http://jubat.us/en/} is an example of distributed streaming machine learning framework. It includes library for streaming machine learning such as regression, classification, recommendation, anomaly detection and graph mining. Local ML model concept is introduced by Jubatus, which means, there can be multiple models running at the same time and they process different sets of data. Using this technique, Jubatus achieves horizontal scalability via horizontal parallelism in partitioning data. 

MapReduce operation is adapted by Jubatus into three fundamental concepts, which are:
\begin{enumerate}
\item \textit{Update}, means process an instance by updating the local model.
\item \textit{Analyze}, means process an instance by applying the local model to it and get the result, such as the predicted class value in classification.
\item \textit{Mix}, merge local model into a mixed model which will be used to update all local models Jubatus.
\end{enumerate}

Jubatus establishes tight coupling between the machine learning library implementation and the underlying distributed SPE. The reason is Jubatus' developers implemented their custom distributed SPE on their own. Fault tolerance is achieved and configured via \textit{jubakeeper} component which utilizes ZooKeeper. 

\textit{stormmoa}\footnote{https://github.com/vpa1977/stormmoa} is a project to combine MOA with Storm, to satisfy the need of scalable implementation of streaming ML frameworks. It uses Storm's Trident abstraction and MOA library to implement OzaBag and OzaBoost\cite{oza_online_2001}. Two variants of the aforementioned ML algo are presented, they are

\begin{enumerate}
\item In-memory-implementation, where each model is only stored in memoery and i not persisted into a storage. This means the model needs to be rebuilt from the beginning when failure happens.
\item Implementation with fault tolerance, where storage is used to persist the model hence stormmoa is able to recover the model upon failure.
\end{enumerate}

Tight integration between MOA and Storm prevents this framework to be extended using other SPEs.