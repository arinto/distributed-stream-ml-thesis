%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               SAMOA - 24pp
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Scalable Advanced Massive Online Analysis}
\label{chapter:samoa}
Scalable Advanced Massive Online Analysis (SAMOA) is the distributed streaming machine learning (ML) framework to perform big data stream mining that we implement in this thesis. SAMOA contains a programming abstraction for distributed streaming algorithm that allows development of new ML algorithms without dealing with the complexity of underlying streaming processing engine (SPE). SAMOA also provides extension points for integration of new SPEs into the system. These features allow SAMOA users to develop distributed streaming ML algorithms once and they can execute the algorithm in multiple SPEs. Section~\ref{sec:high_level_architecture} discusses the high level architecture and the main design goals of SAMOA. Sections ~\ref{sec:samoa_modular_components} and ~\ref{sec:spe_adapter_layer} discuss our implementations to satisfy the main design goals. Section~\ref{sec:storm_integration_to_samoa} presents our work in integrating Storm into SAMOA.

\section{High Level Architecture}
\label{sec:high_level_architecture}

We start the discussion of SAMOA high level architecture by identifying the entities or users that use SAMOA. There are three types of SAMOA users:
\begin{enumerate}
\item Platform users, who need to use ML but they don't want to implement the algorithm. 
\item ML developers, who develop new ML algorithms on top of SAMOA and use the already developed algorithm in SAMOA. 
\item Platform developers, who extend SAMOA to integrate more SPEs into SAMOA.
\end{enumerate}

Moreover, we identify three design goals of SAMOA which are:
\begin{enumerate}
\item Flexibility in term of developing new ML algorithms or reusing existing ML algorithms from existing ML frameworks.
\item Extensibility in term of extending SAMOA with new SPEs.
\item Scalability in term of handling ever increasing amount of data.
\end{enumerate}

\begin{figure}
	\centering
	\includegraphics[scale=0.35]{figs/SAMOA-architecture.pdf}
	\caption{SAMOA High Level Architecture}
	\label{fig:samoa_architecture}
\end{figure}

%Moreover, we identify three design goals of SAMOA which are flexibility, scalability, and extensibility. 
Figure~\ref{fig:samoa_architecture} shows the high-level architecture of SAMOA which attempts to fulfill the aforementioned design goals. The \textit{algorithm} block contains existing distributed streaming algorithms that have been implemented in SAMOA. This block enables platform users to easily use the existing algorithm without worrying about the underlying SPEs.

The \textit{application programming interface}(API) block consists of primitives and components that facilitate ML developers implementing new algorithms. The \textit{ML-adapter} layer allows ML developers to integrate existing algorithms in MOA or other ML frameworks into SAMOA. The API block and ML-adapter layer in SAMOA fulfill the flexibility goal since they allow ML developers to rapidly develop ML algorithms using SAMOA. Section ~\ref{sec:samoa_modular_components} further discusses the modular components of SAMOA and the ML-adapter layer.

Next, the \textit{SPE-adapter} layer supports platform developers in integrating new SPEs into SAMOA. To perform the integration, platform developers should implement the \textit{samoa-SPE} layer as shown in figure~\ref{fig:samoa_architecture}. Currently SAMOA is equipped with two layers: \textit{samoa-S4} layer for S4 and \textit{samoa-Storm} layer for Storm. To satisfy extensibility goal, the SPE-adapter layer decouples SPEs and ML algorithms implementation in SAMOA such that platform developers are able to easily integrate more SPEs into SAMOA. Section ~\ref{sec:spe_adapter_layer} presents more details about this layer in SAMOA.

%Flexibility in SAMOA allows ML developers to perform rapid ML algorithms development on top of SAMOA. To fulfill this goal, SAMOA contains an API block with modular components. Furthermore, \textit{ML-adapter} layer allow ML developers to use existing algorithms from MOA or other ML frameworks in SAMOA. Section ~\ref{sec:samoa_modular_components} further discusses the modular components of SAMOA and ML-adapter layer.

%Other than flexibility in developing new ML algorithms, SAMOA should be flexible in integration with existing streaming ML frameworks such as Massive Online Analysis(MOA). \textit{ML-adapter} layer in figure x is implemented to support this integration. Section xx further discusses the modular components of SAMOA and section xy explains the ML-adapter layer.

%Extensibility in SAMOA allows platform developers to easily integrate more SPEs into SAMOA. To achieve this goal, SAMOA uses \textit{SPE-adapter} layer to decouple SPEs and SAMOA MLs algorithm implementation. Section ~\ref{sec:spe_adapter_layer} presents more details about this layer in SAMOA.

%Usability in SAMOA enables data scientists and researchers to use SAMOA intuitively. To satisfy this design goal, SAMOA uses similar command line structure that used by MOA. This means, scientists and researchers who is familiar with MOA can easily use SAMOA. Section xx further explains SAMOA command line structure.

The last goal, scalability, implies that SAMOA should be able to scale to cope ever increasing amount of data. To fulfill this goal, SAMOA utilizes modern SPEs to execute its ML algorithms. The reason for using modern SPEs such as Storm and S4 in SAMOA is that they are designed to provide horizontal scalability to cope with a high amount of data. Currently SAMOA is able to execute on top of Storm and S4. As this work focuses on integration of SAMOA with Storm, section ~\ref{sec:storm_integration_to_samoa} discusses about this integration.

%We develop SAMOA using Java because: TODO but low priority

\section{SAMOA Modular Components}
\label{sec:samoa_modular_components}
This section discusses SAMOA modular components and APIs that allow ML developers to perform rapid algorithm development. The components are:  \textit{processing item}(PI), \textit{processor}, \textit{stream}, \textit{content event}, \textit{topology} and \textit{task}.

%Figure x shows the class diagram of SAMOA modular components. 

\subsection{Processing Item and Processor}
\label{sec:samoa_pi_and_proc}
A \textit{processing item}(PI) in SAMOA is a unit of computation element that executes some part of the algorithm on \textit{a specific SPE}. This means, each SPE in SAMOA has different concrete implementation of PIs. The SPE-adapter layer handles the instantiation of PIs. There are two types of PI, \textit{entrance PI} and \textit{normal PI}. An entrance PI converts data from external source into instances or independently generates instances. Then, it sends the instances to the destination PI via the corresponding stream using the correct type of content event. A normal PI consumes content events from incoming stream, processes the content events, and it may send the same content events or new content events to outgoing streams. ML developers are able to specify the \textit{parallelism hint}, which is the number of \textit{runtime PI} during SAMOA execution as shown in figure~\ref{fig:parallelism_hint}. A runtime PI is an actual PI that is created by the underlying SPE during execution. We implement PIs as a Java interface and SAMOA dynamically instantiates the concrete class implementation of the PI based on the underlying SPE.

\begin{figure}
	\centering
	\includegraphics[scale=0.35]{figs/parallelism_hint.pdf}
	\caption{Parallelism Hint in SAMOA}
	\label{fig:parallelism_hint}
\end{figure}

A PI uses composition to contain its corresponding processor and streams. A \textit{processor} contains the actual logic of the algorithm implemented by ML developers. Furthermore, a processor is reusable which allows ML developers to use the same implementation of processors in more than one ML algorithm implementations. The separation between PIs and processors allows ML developers to focus on developing ML algorithm without worrying about the SPE-specific implementation of PIs.

\subsection{Stream and Content Event}
\label{sec:stream_and_content_event}
%It has a similar definition with the convention in section~\ref{sec:paralellism_type}.
A \textit{stream} is a connection between a PI into its corresponding destination PIs. ML developers view streams as connectors between PIs and mediums to send \textit{content event} between PIs. A \textit{content event} wraps the data transmitted from a PI to another via a stream. Moreover, similar to processors, content events are reusable. ML developers can reuse a content event in more than one algorithm.

\begin{figure}
	\centering
	\includegraphics[scale=0.35]{figs/stream-grouping-instantiation.pdf}
	\caption{Instatiation of a Stream and Examples of Groupings in SAMOA}
	\label{fig:stream_grouping_instantiation}
\end{figure}

Refer to figure~\ref{fig:stream_grouping_instantiation}, we define a \textit{source PI} as a PI that sends content events through a stream. A \textit{destination PI} is a PI that receives content event via a stream. ML developers instantiate a stream by associating it with exactly one source PI. When destination PIs want to connect into a stream, they need to specify the \textit{grouping} mechanism which determines how the stream routes the transported content events. Currently there are three grouping mechanisms in SAMOA: 
\begin{itemize}
\item \textit{Shuffle} grouping, which means the stream routes the content events in a round-robin way among the corresponding runtime PIs. This means each runtime PI receives the same number of content events from the stream.
\item \textit{All} grouping, which means the stream replicates the content events and routes them to all corresponding runtime PIs.
\item \textit{Key} grouping, which means the stream routes the content event based on the \textit{key} of the content event, i.e. the content events with the same value of key are always routed by the stream into the same runtime PI.
\end{itemize}

We design streams as a Java interface. Similar to processing items, the streams are dynamically instantiated by SAMOA based on the underlying SPEs hence ML developers do not need to worry about the streams and groupings implementation. We design content events as a Java interface and ML developers need to provide a concrete implementation of content events especially to generate the necessary \textit{key} to perform key grouping.

\subsection{Topology and Task}
\label{sec:samoa_topology}
A \textit{topology} is a collection of connected processing items and streams. It represents a network of components that process incoming data streams. A distributed streaming ML algorithm implemented on top of SAMOA corresponds to a topology. 

A \textit{task} is a machine learning related activity such as performing a specific evaluation for a classifier. Example of a task is prequential evaluation task i.e. a task that uses each instance for testing the model performance and then it uses the same instance to train the model using specific algorithms. A task corresponds also to a topology in SAMOA.

Platform users basically use SAMOA's tasks. They specify what kind of task they want to perform and SAMOA automatically constructs a topology based on the corresponding task. Next, the platform users need to identify the SPE cluster that is available for deployment and configure SAMOA to execute on that cluster. Once the configuration is correct, SAMOA deploys the topology into the configured cluster seamlessly and platform users could observe the execution results through the dedicated log files of the execution. Moving forward, SAMOA should incorporate proper user interface similar to Storm UI to improve experience of platform users in using SAMOA. 

\subsection{ML-adapter Layer}	
\label{sec:ml_adapter_layer}
The ML-adapter layer in SAMOA consists of classes that wrap ML algorithm implementations from other ML frameworks. Currently SAMOA has a wrapper class for MOA algorithms or learners, which means SAMOA can easily utilizes MOA learners to perform some tasks. SAMOA does not change the underlying implementation of the MOA learners therefore the learners still execute in sequential manner on top of SAMOA underlying SPE. 

SAMOA is implemented in Java, hence ML developers can easily integrate Java-based ML frameworks. For other frameworks not in Java, ML developers could use available Java utilities such as JNI to extend this layer.

\subsection{Putting All the Components Together}
\label{sec:putting_all_together}
ML developers design and implement distributed streaming ML algorithms with the abstraction of processors, content events, streams and processing items. Using these modular components, they have flexibility in implementing new algorithms by reusing existing processors and content events or writing the new ones from scratch. They have also flexibility in reusing existing algorithms and learners from existing ML frameworks using ML-adapter layer.

Other than algorithms, ML developers are also able to implement tasks also with the same abstractions. Since processors and content events are reusable, the topologies and their corresponding algorithms are also reusable.  This implies, they have also flexibility in implementing new task by reusing existing algorithms and components, or by writing new algorithms and components from scratch.

\section{SPE-adapter Layer}
\label{sec:spe_adapter_layer}
The SPE-adapter layer decouples the implementation of ML algorithm and the underlying SPEs. This decoupling facilitates platform developers to integrate new SPEs into SAMOA. Refer to the simplified class diagram of SPE-adapter layer in figure~\ref{fig:spe_adapter_simplified}, SPE-adapter layer uses the abstract factory pattern to instantiate the appropriate SAMOA components based on the underlying SPEs. \verb;ComponentFactory; is the interface representing the factory and this factory instantiates objects that implement \verb;Component; interface. The \verb;Component; in this case refers to SAMOA's procesing items and streams. Platform developers should implement the concrete implementation of both \verb;ComponentFactory; and \verb;Component;. Current SAMOA implementation has factory and components implementation for S4 and Storm.

\begin{figure}
	\centering
	\includegraphics[scale=0.6]{figs/spe_adapter_simplified.pdf}
	\caption{Simplified Class Diagram of SPE-adapter Layer}
	\label{fig:spe_adapter_simplified}
\end{figure}

This design makes SAMOA platform agnostic which allows ML developers and platform users to use SAMOA with little knowledge of the underlying SPEs. Furthermore, platform developers are able to integrate new SPEs into SAMOA without any knowledge of the available algorithms in SAMOA. 

\section{Storm Integration to SAMOA}
\label{sec:storm_integration_to_samoa}
This section explains our work in integrating Storm into SAMOA through the aforementioned SPE-adapter layer. To start the explanation, section~\ref{sec:storm_overview} presents some basic knowledge of Storm that related to the SPE-adapter layer. Section~\ref{sec:proposed_design} discusses our proposed design in adapting Storm components into SAMOA. 
%The next section, section kk presents our design in deploying SAMOA on actual Storm cluster. 

\subsection{Overview of Storm}
\label{sec:storm_overview}
Storm is a distributed streaming computation framework that utilizes MapReduce-like programming model for streaming data. Storm main use case is to perform real-time analytics for streaming data. For example, Twitter uses Storm\footnote{http://www.slideshare.net/KrishnaGade2/storm-at-twitter} to discover emerging stories or topics, to perform online learning based on tweet features for ranking of search results, to perform realtime analytics for advertisement and to process internal logs. The main advantage of Storm over Hadoop MapReduce (MR) is its flexibility in handling stream processing. In fact, Hadoop MR has complex and error-prone configuration when it is used for handling streaming data. Storm provides at-least-once message processing. It is designed to scale horizontally. Storm does not have intermediate queues which implies less operational overhead. Moreover, Storm promises also to be a platform that "just works". 

The fundamental primitives of Storm are \textit{streams}, \textit{spouts}, \textit{bolts}, and \textit{topologies}. A stream in Storm is an unbounded sequence of \textit{tuple}. A tuple is a list of values and each value can be any type as long as the values are serializable. Tuples in Storm are dynamically typed which means the type of each value need not be declared. Example of a tuple is \verb;{height, weight}; tuple which consists of \verb;height; value and \verb;weight; value. These values logically should be a \verb;double; or \verb;float; but since they are dynamic types, the tuple can contain any data type. Therefore, it is the responsibility of Storm users to ensure that a tuple contains correct type as intended.  

A spout in Storm is a source of streams. Spouts typically read from external sources such as kestrel/kafka queues, http server logs or Twitter streaming APIs. A bolt in Storm is a consumer of one or more input streams. Bolts are able to perform several functions for the consumed input stream such as filtering of tuples, aggregation of tuples, joining multiple streams, and communication with external entities (such as caches or databases). Storm utilizes \textit{pull} model in transferring tuple between spouts and bolts i.e. each bolt pulls tuples from the source components (which can be other bolts or spouts). This characteristic implies that loss of tuples only happens in spouts when they are unable to keep up with external event rates.

\begin{figure}
	\centering
	\includegraphics[scale=0.35]{figs/storm_topology.pdf}
	\caption{Example of Storm Topology}
	\label{fig:storm_topology}
\end{figure}

%TODO: check Italics
A topology in Storm is a network of spouts, bolts and streams that forms a directed-acyclic-graph. Figure~\ref{fig:storm_topology} shows the example of a topology in Storm. There are two spouts(\verb;S1; and \verb;S2;) and five bolts(\verb;B1; to \verb;B5;). A spout can send tuples to more than one different bolts. Moreover, one or more streams can arrive to a single bolt. Refer to figure~\ref{fig:storm_topology}, bolt \verb;B1; processes stream A and produces stream B. Bolt \verb;B4; consumes stream B and communicates with external storage to store useful information such as the current state of the bolt. 

\begin{figure}
	\centering
	\includegraphics[scale=0.3]{figs/storm_cluster.pdf}
	\caption{Storm Cluster}
	\label{fig:storm_cluster}
\end{figure}

Figure~\ref{fig:storm_cluster} shows a Storm cluster which consists of these following components: one \textit{nimbus}, two \textit{supervisors}, three \textit{workers} for each supervisor and a ZooKeeper cluster. A nimbus is the master node in Storm that acts as entry point to submit   topologies and code (packaged as jar) for execution on the cluster. It distributes the code around the cluster via supervisors. A supervisor runs on slave node and it coordinates with ZooKeeper to manage the workers. A worker in Storm corresponds to a JVM process that executes a subset of a topology. Figure~\ref{fig:storm_cluster} shows three workers (\verb;w1;, \verb;w2;, \verb;w3;) in each supervisor. A worker comprises of several \textit{executors} and \textit{tasks}. An executor corresponds to a thread spawned by a worker and it consists of one or more tasks from the same type of bolt or spout. A task performs the actual data processing based on spout or bolt implementations. Storm cluster uses ZooKeeper cluster to perform coordination functionalities by storing the configurations of Storm workers.

To determine the partitioning of streams among the corresponding bolt's tasks, Storm utilizes stream groupings mechanisms. Storm allows users to provide their own implementation and grouping. Furthermore, Storm also provides several default grouping mechanisms, such as:
\begin{enumerate}
\item \verb;shuffle; grouping, in which Storm performs randomized round robin. Shuffle grouping results in the same number of tuples received by each task in the corresponding bolt. 
\item \verb;fields; grouping, which partitions the stream based on the values of one or more fields specified by the grouping. For example, a stream can be partitioned by an "id" value hence a tuple with the same id value will always arrive to the same task. 
\item \verb;all; grouping, which replicates the tuple to all tasks in the corresponding bolt.
\item \verb;global; grouping, which sends all tuples in a stream in to only one task. 
\item \verb;direct; grouping, which sends each tuple directly to a task based on the provided task id.
\end{enumerate}

\subsection{Proposed Design}
\label{sec:proposed_design}

\begin{figure}
	\centering
	\includegraphics[scale=0.6]{figs/samoa_storm.pdf}
	\caption{samoa-storm Class Diagram}
	\label{fig:samoa_storm}
\end{figure}

In order to integrate Storm components to SAMOA, we need to establish relation between Storm classes and the existing SAMOA components. Figure~\ref{fig:samoa_storm} shows the class diagram of our proposed implementation. In this section we refer our implementation in integrating Storm into SAMOA as \textit{samoa-storm}. Samoa-storm consists of concrete implementation of processing items, they are \verb;StormEntranceProcessingItem; and \verb;StormProcessingItem;. Both of them also implement \verb;StormTopologyNode; to provide common functionalities in creating \verb;StormStream; and in generating unique identification. A \verb;StormEntranceProcessingItem; corresponds with a spout with composition relation. Similarly, a \verb;StormProcessingItem; corresponds to a bolt with composition relation. In both cases, composition is favored to inheritance because Storm differentiates between the classes that help constructing the topology and the classes that execute in the cluster. If PIs are subclasses of Storm components (spouts or bolts), we will need to create extra classes to help constructing the topology and it may increase the complexity of the layer.

In samoa-storm, \verb;StormEntranceProcessingItem; and \verb;StormProcessingItem; are the ones which construct topology. And their corresponding Storm components, \verb;StormEntranceSpout; and \verb;ProcesingItemBolt; are the ones which execute in the cluster.

A \verb;StormStream; is an abstract class that represents implementation of SAMOA stream. It has an abstract method called \verb;put; to send content events into destination PI. Samoa-storm uses abstract class because spouts and bolts have a different way of sending tuples into destination bolts. A \verb;StormStream;  basically is a container for stream identification strings and Storm specific classes to send tuples into destination PIs. Each \verb;StormStream; has a string as its unique identification(ID). However, it needs to expose two types of identifications: input ID and output ID. Samoa-storm uses the input ID to connect a bolt into another bolt or a spout when building a topology. An input ID comprises of the unique ID of processing item in which the stream is associated to, and the stream's unique ID. The reason why an input ID consists of two IDs is that Storm needs both IDs to uniquely identify connection of a bolt into a stream. An output ID only consists of the unique ID of \verb;StormStream; and Storm uses the output ID to declare a stream using Storm APIs. 

A \verb;StormSpoutStream; is a concrete implementation of \verb;StormStream; that is used by a \verb;StormEntranceProcessingItem; to send content events. The \verb;put; method in \verb;StormSpoutStream; puts the content events into a \verb;StormEntranceSpout;'s queue and since Storm utilizes pull model, Storm starts sending the content event by clearing this queue. On the other hand, a \verb;StormProcessingItem; uses a \verb;StormBoltStream; to send content events to another \verb;StormProcessingItem;. \verb;StormBoltStream; utilizes bolt's \verb;OutputCollector; to perform this functionality.

%\subsection{SAMOA Deployment in Storm}
%\label{sec:samoa_deployment_in_storm}
%SAMOA artifacts related to Storm is a Storm-specific jar file that contains all ML algorithms implementation and the adapter implementation which is presented in section~\ref{sec:adapting_samoa_components_to_storm}.