%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                              Decision Tree Induction - 24-32pp
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Decision Tree Induction}
\label{chapter:decision_tree_induction}

This chapter presents the existing algorithms that are used in our work. Section~\ref{sec:basic_algorithm} presents the basic decision tree induction. The following section, section~\ref{sec:additional_techniques}, discusses the necessary additional techniques to make the basic algorithm ready for production environments. Section~\ref{sec:stream_dec_tree_induction} presents the corresponding algorithm for streaming settings. In addition, section~\ref{sec:extensions_of_vdft} explains some extensions for the streaming decision tree induction algorithm.

%Decision tree induction is a divide-and-conquer algorithm in building a model for a classifier. The algorithm builds the model from a set of training data and the resulting model resembles a decision tree. This type of algorithm is often referred as \textit{learner}. Once the model is available, the classifier uses the model to predict the class value of newly arriving datum. In this report, we refer each datum in the data as \textit{instance}. 

A decision tree consists of \textit{nodes}, \textit{branches} and \textit{leaves} as shown in Figure~\ref{fig:decision_tree_components}. A \textit{node} consists of a question regarding a value of an attribute, for example node $n$ in Figure~\ref{fig:decision_tree_components} has a question: "is attr\_one greater than 5?". We refer to this kind of node as \textit{split-node}. \textit{Branch} is a connection between nodes that is established based on the answer of its corresponding question. The aforementioned node \textit{n} has two branches: "True" branch and "False" branch. Leaf is an end-point  in the tree. The decision tree uses the \textit{leaf} to predict the class of given data by utilizing several predictor functions such as majority-class, or naive Bayes classifier. Sections~\ref{sec:basic_algorithm} to ~\ref{sec:additional_techniques} discuss the basic tree-induction process in more details.

\begin{figure}
	\centering
	\includegraphics[scale=0.28]{figs/decision_tree_components.pdf}
	\caption{Components of Decision Tree}
	\label{fig:decision_tree_components}
\end{figure}
\begin{flushleft}

\end{flushleft}
\section{Basic Algorithm}
\label{sec:basic_algorithm}
Algorithm~\ref{alg:decision_tree_induction} shows the generic description of decision tree induction. The decision tree induction algorithm begins with an empty tree(line 1). Since this algorithm is a recursive algorithm, it needs to have the termination condition shown in line 2 to 4. If the algorithm does not terminate, it continues by inducing a root node that considers the entire dataset for growing the tree. For the root node, the algorithm processes each datum in the dataset $D$ by iterating over all available attributes (line 5 to 7) and choosing the attribute with best information-theoretic criteria ($a_{best}$ in line 8). Section~\ref{sec:information_gain} and ~\ref{sec:gain_ratio} further explain the calculation of the criteria. After the algorithms decides on $a_{best}$, it creates a split-node that uses $a_{best}$ to test each datum in the dataset (line 9). This testing process induces sub-dataset $D_v$ (line 10). The algorithm continues by recursively processing each sub-dataset in $D_v$. This recursive call produces sub-tree $Tree_v$ (line 11 and 12). The algorithm then attaches $Tree_v$ into its corresponding branch in the corresponding split-node (line 13).

\begin{algorithm}
\caption{DecisionTreeInduction(D)}
\begin{algorithmic}[1]
\REQUIRE $D$, which is attribute-valued dataset
\STATE $Tree$ = \{\}
\IF{ $D$ is "pure" OR we satisfy other stopping criteria}
\STATE terminate
\ENDIF
\FORALL{attribute $a \in D$}
\STATE Compare information-theoretic criteria if we split on $a$
\ENDFOR
\STATE Obtain attribute with best information-theoretic criteria, $a_{best}$
\STATE Add a split-node that splits on $a_{best}$ into 
\STATE Induce sub-dataset of $D$ based on $a_{best}$ into $D_v$
\FORALL{$D_v$}
\STATE $Tree_v$ = DecisionTreeInduction($D_v$)
\STATE Attach $Tree_v$ into corresponding branch in the split-node.
\ENDFOR
\RETURN $Tree$
\end{algorithmic}
\label{alg:decision_tree_induction}
\end{algorithm}

\subsection{Sample Dataset}
\label{sec:sample_dataset}
Given the weather dataset~\cite{witten2005data} in table~\ref{tab:weather_dataset}, we want to predict whether we should play or not play outside. The attributes are \textit{Outlook}, \textit{Temperature}, \textit{Humidity}, \textit{Windy }and \textit{ID code}. Each attribute has its own possible values, for example \textit{Outlook} has three possible values: \textit{sunny}, \textit{overcast} and \textit{rainy}. The output class is \textit{Play} and it has two values: \textit{yes} and \textit{no}. In this example, the algorithm uses all the data for training and building the model.

\setlength{\tabcolsep}{12pt}
\begin{table}
\centering
\begin{tabular}{*{5}{l}r}
	\hline
	ID code	& Outlook	& Temperature	& Humidity	& Windy	& Play	\\
	\hline
	a		& sunny		& hot			& high		& false & no	\\
	b       & sunny     & hot			& high		& true	& no	\\
	c		& overcast	& hot			& high		& false	& yes   \\
	d		& rainy		& mild			& high		& false	& yes	\\
	e		& rainy		& cool			& normal	& false	& yes	\\
	f		& rainy		& cool			& normal	& true	& no	\\
	g		& overcast	& cool			& normal	& true	& yes	\\
	h		& sunny		& mild			& high		& false	& no	\\
	i		& sunny		& cool			& normal	& false	& yes	\\
	j		& rainy		& mild			& normal	& false	& yes	\\
	k		& sunny		& mild			& normal	& true	& yes	\\
	l		& overcast	& mild			& high		& true	& yes	\\
	m		& overcast	& hot			& normal	& false	& yes	\\
	n		& rainy		& mild			& high		& true	& no	\\
	\hline
\end{tabular}
\caption{Weather Dataset}
\label{tab:weather_dataset}
\end{table}

Ignoring the \textit{ID code} attribute, we have four possible splits in growing the tree's root shown in Figure~\ref{fig:possible_splits}. Section~\ref{sec:information_gain} and ~\ref{sec:gain_ratio} explain how the algorithm chooses the best attribute.

\begin{figure}
	\centering
	\includegraphics[scale=0.56]{figs/possible_splits.pdf}
	\caption{Possible Splits for the Root in Weather Dataset}
	\label{fig:possible_splits}
\end{figure}

\subsection{Information Gain}
\label{sec:information_gain}
The algorithm needs to decide which split should be used to grow the tree. One option is to use the attribute with the highest \textit{purity measure}. The algorithm measures the attribute purity in term of \textit{information value}. To quantify this measure, the algorithm utilizes \textit{entropy} formula. Given a random variable that takes $c$ values with probabilities $p_1$, $p_2$, ... $p_c$, the algorithm calculates the information value with this following entropy formula:

\[\sum^{c}_{i=1}{-p_ilog_2p_i}\]

Refer to Figure~\ref{fig:possible_splits}, the algorithm derives the information values for \textit{Outlook} attribute with following steps:
\begin{enumerate}
\item \textit{Outlook} has three outcomes: sunny, overcast and rainy. The algorithm needs to calculate the information values for each outcome.
\item Outcome \textit{sunny} has two occurrences of output class \textit{yes}, and three occurrences of \textit{no}. The information value of \textit{sunny} is $info([2,3]) = -p_1log_2p_1-p_2log_2p_2$ where $p_1$ is the probability of \textit{yes} (with value of $\frac{2}{2+3} = \frac{2}{5}$) and $p_2$ is the probability of "no" in sunny outlook (with value of $\frac{3}{2+3} = \frac{3}{5}$). The algorithm uses both $p$ values into the entropy formula to obtain \textit{sunny}'s information value: $info([2,3])= -\frac{2}{5}log_\frac{2}{5}-\frac{3}{5}log_2\frac{3}{5}=0.971$ bits.
\item The algorithm repeats the calculation for other outcomes. Outcome \textit{overcast} has four \textit{yes} and zero \textit{no}, hence its information value is: $info([4,0]) = 0.0$ bits. Outcome \textit{rainy} has three \textit{yes} and two \textit{no}, hence its information values is:  $info[(3,2)]=0.971$ bits.
\item The next step for the algorithm is to calculate the expected amount of information when it chooses \textit{Outlook} to split, by using this calculation: $info([2,3],[4,0],[3,2])=\frac{5}{14}info([2,3])+\frac{4}{14}info([4,0])+\frac{5}{14}info([3,2])=0.693$ bits.
\end{enumerate}

The next step is to calculate the information gain obtained by splitting on a specific attribute. The algorithm obtains the gain by subtracting the entropy of splitting on a specific attribute with the entropy of no-split case. 

Continuing the \textit{Outlook} attribute sample calculation, the algorithm calculates entropy for no-split case: $info([9,5])=0.940$ bits. Hence, the information gain for \textit{Outlook} attribute is $gain(Outlook) = info([9,5])-info([2,3],[4,0],[3,2])=0.247$ bits.

Refer to the sample case in Table~\ref{tab:weather_dataset} and Figure~\ref{fig:possible_splits}, the algorithm produces these following gains for each split:
\begin{itemize}
\item $gain(Outlook) = 0.247$ bits
\item $gain(Temperature) = 0.029$ bits
\item $gain(Humidity) = 0.152$ bits
\item $gain(Windy) = 0.048$ bits
\end{itemize}

\textit{Outlook} has the highest information gain, hence the algorithm chooses attribute \textit{Outlook} to grow the tree and split the node. The algorithm repeats this process until it satisfies one of the terminating conditions such as the node is pure (i.e the node only contains one type of class output value).

\subsection{Gain Ratio}
\label{sec:gain_ratio}

The algorithm utilizes \textit{gain ratio} to reduce the tendency of information gain to choose attributes with higher number of branches. This tendency causes the model to overfit the training set, i.e. the model performs very well for the learning phase but it perform poorly in predicting the class of unknown instances. The following example discusses gain ratio calculation.

Refer to table~\ref{tab:weather_dataset}, we include \textit{ID code} attribute in our calculation. Therefore, the algorithm has an additional split possibility as shown in Figure~\ref{fig:id_code_split}.

\begin{figure}
	\centering
	\includegraphics[scale=0.4]{figs/id_code_split.pdf}
	\caption{Possible Split for \textit{ID code} Attribute}
	\label{fig:id_code_split}
\end{figure}

This split will have entropy value of 0, hence the information gain is 0.940 bits. This information gain is higher than \textit{Outlook}'s information gain and the algorithm will choose \textit{ID code} to grow the tree. This choice causes the model to overfit the training dataset. To alleviate this problem, the algorithm utilizes \textit{gain ratio}.

The algorithm calculates the gain ratio by including the number and size of the resulting daughter nodes but ignoring any information about the daughter nodes' class distribution. Refer to \textit{ID code} attribute, the algorithm calculates the gain ratio with the following steps:

\begin{enumerate}
\item The algorithm calculates the information value for \textit{ID code} attribute while ignoring the class distribution for the attribute: $info[(1,1,1,1....1)]=-\frac{1}{14}log_2\frac{1}{14}\times14=3.807$ bits.
\item Next, it calculates the gain ratio by using this formula: $gain\_ratio(ID\,code)=\frac{gain(ID\,code)}{info[(1,1,1,1...1)]}=\frac{0.940}{3.807}=0.247$.
\end{enumerate}

For \textit{Outlook} attribute, the corresponding gain ratio calculation is:
\begin{enumerate}
\item Refer to Figure~\ref{fig:possible_splits}, \textit{Outlook} attribute has five class outputs in \textit{sunny}, four class outputs in \textit{overcast}, and five class outputs in \textit{rainy}. Hence, the algorithm calculates the information value for \textit{Outlook} attribute while ignoring the class distribution for the attribute: $info[(5,4,5)]=-\frac{5}{14}log_2\frac{5}{14}-\frac{4}{14}log_2\frac{4}{14}-\frac{5}{14}log_2\frac{5}{14}=1.577$ bits.
\item $gain\_ratio(Outlook)=\frac{gain(Outlook)}{info[(5,4,5)]}=\frac{0.247}{1.577}=0.157$.
\end{enumerate}

The gain ratios for every attribute in this example are:
\begin{itemize}
\item $gain\_ratio(ID\,code)=0.247$
\item $gain\_ratio(Outlook)=0.157$
\item $gain\_ratio(Temperature)=0.019$
\item $gain\_ratio(Humidity)=0.152$
\item $gain\_ratio(Windy)=0.049$
\end{itemize}

Based on above calculation, the algorithm still chooses \textit{ID code} to split the node but the gain ratio reduces the \textit{ID code}'s advantages to the other attributes. \textit{Humidity} attribute is now very close to \textit{Outlook} attribute because it splits the tree into less branches than \textit{Outlook} attribute splits.

\section{Additional Techniques in Decision Tree Induction}
\label{sec:additional_techniques}

Unfortunately, information gain and gain ratio are not enough to build a decision tree that suits production settings. One example of well-known decision tree implementation is C4.5~\cite{quinlan_c4.5:_1993}. This section explain further techniques in C4.5 to make the algorithm suitable for production settings. 

Quinlan, the author of C4.5, proposes \textit{tree pruning} technique to avoid overfitting by reducing the number of nodes in the tree. C4.5 commonly performs this technique in a single bottom-up pass after the tree is fully grown. Quinlan introduces \textit{pessimistic pruning} that estimates the error rate of a node based on the estimated errors of its sub-branches. If the estimated error of a node is less than its sub-branches' error, then pessimistic pruning uses the node to replace its sub-branches.

The next technique is in term of continuous attributes handling. Continuous attributes require the algorithm to choose threshold values to determine the number of splits. To handle this requirement, C4.5 utilizes only the information gain technique in choosing the threshold. For choosing the attribute, C4.5 still uses the information gain and the gain ratio techniques altogether. 

C4.5 also has several possibilities in handling missing attribute values during learning and testing the tree. There are three scenarios when C4.5 needs to handle the missing values properly, they are:
\begin{itemize}
\item{When comparing attributes to split and some attributes have missing values.}
\item{After C4.5 splits a node into several branches, a training datum with missing values arrives into the split node and the split node can not associate the datum with any of its branches.} 
\item{When C4.5 attempts to classify a testing datum with missing values but it can not associate the datum with any of the available branches in a split node.}
\end{itemize}

Quinlan presents a coding scheme in~\cite{quinlan_decision_1987} and ~\cite{quinlan_unknown_1989} to deal with each of the aforementioned scenarios. Examples of the coding scheme are: (I) to ignore training instances with missing values and (C) to substitute the missing values with the most common value for nominal attributes or with the mean of the known values for the numeric attributes. 

\section{Very Fast Decision Tree (VFDT) Induction}
\label{sec:stream_dec_tree_induction}
We briefly described in section~\ref{sec: streaming_decision_tree} Very Fast Decision Tree (VFDT)~\cite{domingos_mining_2000} which is the pioneer of streaming decision tree induction. VFDT fulfills the necessary requirements in handling data streams in an efficient way. The previous streaming decision tree algorithms that introduced before VFDT does not have this characteristic. VFDT is one of the fundamental concepts in our work, hence this section further discusses the algorithm. 

This section refers the resulting model from VFDT as \textit{Hoeffding tree} and the induction algorithm as \textit{Hoeffding tree induction}. We refer to data as \textit{instances}, hence we also refer datum as a single instance. Moreover, this section refers the whole implementation of VFDT as \textit{VFDT}.

\begin{algorithm}
\caption{HoeffdingTreeInduction($E$, $HT$)}
\begin{algorithmic}[1]
\REQUIRE $E$ is a training instance
\REQUIRE $HT$ is the current state of the decision tree
\STATE Use $HT$ to sort $E$ into a leaf $l$
\STATE Update sufficient statistic in $l$
\STATE Increment the number of instances seen at $l$ (which is $n_l$)
\IF{$n_l$ $mod$ $n_{min}$ $= 0$ {\bf and} not all instances seen at $l$ belong to the same class}
\STATE For each attribute, compute $\overline{G}_{l}(X_{i})$
\STATE Find $X_{a}$, which is the attribute with highest $\overline{G}_{l}$
\STATE Find $X_{b}$, which is the attribute with second highest $\overline{G}_{l}$
\STATE Compute Hoeffding bound $\epsilon = \sqrt{\frac{R^{2}\ln(1/\delta)}{2n_{l}}}$
\IF{$X_{a} \ne X_{\emptyset}$ {\bf and} ($\overline{G}_{l}(X_{a}) - \overline{G}_{l}(X_{b}) > \epsilon$ {\bf or} $\epsilon < \tau$)}
\STATE Replace $l$ with a split-node on $X_{a}$
\FORALL{branches of the split}
\STATE Add a new leaf with derived sufficient statistic from the split node
\ENDFOR
\ENDIF
\ENDIF
\end{algorithmic}
\label{alg:ht}
\end{algorithm}

Algorithm~\ref{alg:ht} shows the generic description of Hoeffding tree induction. During the learning phase, VFDT starts Hoeffding tree with only a single node. For each training instance $E$ that arrives into the tree, VFDT invokes Hoeffding tree induction algorithm. The algorithms starts by sorting the instance into a leaf $l$(line 1). This leaf is a \textit{learning leaf}, therefore the algorithm needs to update the sufficient statistic in $l$(line 2). In this case, the sufficient statistic is the class distribution for each attribute value. The algorithms also increment the number of instances ($n_l$) seen at lea $l$ based on $E$'s weight (line 3). One instance is not significant enough to grow the tree, therefore the algorithm only grows the tree every certain number of instances ($n_{min}$). The algorithm does not grow the trees if all the data seen at $l$ belong to the same class. Line 4 shows these two conditions to decide whether to grow or not to grow the tree.

In this algorithm, growing the tree means attempting to split the node. To perform the split, the algorithm iterates through each attribute and calculate the corresponding information-theoretic criteria($\overline{G}_{l}(X_{i})$ in line 5). It also computes the information-theoretic criteria for no-split scenario ($X_{\emptyset}$). The authors of VFDT refers this inclusion of no-split scenario with term \textit{pre-pruning}. 

The algorithm then chooses the best($X_{a}$) and the second best($X_{b})$) attributes based on the criteria (line 6 and 7). Using these chosen attributes, the algorithm computes the Hoeffding bound to determine whether it needs to split the node or not. Line 9 shows the complete condition to split the node. If the best attribute is the no-split scenario ($X_{\emptyset}$), then the algorithm does not perform the split. The algorithm uses tie-breaking $\tau$ mechanism to handle the case where the difference of information gain between $X_{a}$ and $X_{b})$ is very small($ \Delta \overline{G}_{l} <  \epsilon < \tau$). If the algorithm splits the node, then it replaces the leaf $l$ with a split node and it creates the corresponding leaves based on the best attribute (line 10 to 13).

To calculate Hoeffding bound, the algorithm uses these following parameters:
\begin{itemize}
\item $r$ = real-valued random variable,with range $R$.
\item $n$ = number of independent observations have been made.
\item $\bar{r}$ = mean value computed from $n$ independent observations.
\end{itemize}

The Hoeffding bound determines that the true mean of the variable is at least $\bar{r}-\epsilon$ with probability $1-\delta$. And, $\epsilon$ is calculated with this following formula:

\[\epsilon = \sqrt{\frac{R^{2}ln(1/\delta)}{2n}}\]

What makes Hoeffding bound attractive is its ability to give the same results regardless the probability distribution generating the observations. This attractiveness comes with one drawback which is different number of observations to reach certain values of $\delta$ and $\epsilon$ depending on the probability distributions of the data.

VFDT has no termination condition since it is a streaming algorithm. The tree may grow infinitely and this contradicts one of the requirements for algorithm for streaming setting (require limited amount of memory). To satisfy the requirement of limited memory usage, the authors of VFDT introduce node-limiting technique. This technique calculates the \textit{promise} for each active learning leaf $l$. A promise of an active learning leaf in VFDT is defined as an estimated upper-bound of the error reduction achieved by keeping the leaf active. Based on the promise, the algorithm may choose to deactivate leaves with low promise when the tree reaches the memory limit. Although the leaves are inactive, VFDT still monitors the promise for each inactive leaf. The reason is that VFDT may activate the inactive leaves when their promises are higher than currently active leaves' promises. Besides the node-limiting technique, the VFDT authors introduce also poor-attribute-removal technique to reduce VFDT memory usage. VFDT removes the attributes that does not look promising while splitting, hence the statistic associated with the removed attribute can be deleted from the memory.

\section{Extensions of VFDT}
\label{sec:extensions_of_vdft}
Since the inception of VFDT, researchers have proposed many enhancements based on VFDT. Hulten et. al.~\cite{hulten_mining_2001} presents Concept-adaptive VFDT (CVFDT) which handles changes in the data stream input characteristics. CVFDT utilizes a sliding-window of data from data stream to monitor the changes and adjust the decision tree accordingly. To monitor the changes, CVFDT periodically determines the best splitting candidates at every previous splitting node. If one of the candidates is better than current attribute to split the node, then either the original result from the current attribute is incorrect or changes have occurred. When these cases happen, CVFDT adjusts the decision tree by deactivating some existing subtrees and growing new alternative subtrees.

Gama et. al.~\cite{gama_accurate_2003} propose VFDTc to handle continuous numeric attribute and to improve the accuracy of VFDT. VFDTc handles continuous numeric attribute by maintaining a binary tree for each observed continuous attribute. It represents split point as a node in the binary tree. A new split point is only added by VFDTc into the binary tree when the number of incoming data in each subsets is higher than a configurable constant. The binary tree allows efficient calculation of the merit in each split-point. Furthermore, VFDTc improves the accuracy of VFDT by utilizing custom classifiers at VFDTc tree leaves. In this work, the authors use naive Bayes classifiers and majority class classifiers. 

Jin and Agrawal~\cite{jin_efficient_2003} present an alternative in processing numerical attributes by using a numerical interval pruning (NIP) approach. This technique allows faster execution time. In addition, the authors also propose a method to reduce the required sample sizes to reach a given bound on the accuracy. This method utilizes the properties of gain function entropy and gini.