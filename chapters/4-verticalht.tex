%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               Distributed Streaming Decision Tree Induction - 24pp
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Distributed Streaming Decision Tree Induction}
\label{chapter:distributed_streaming_decision_tree_induction}
This chapter presents Vertical Hoeffding Tree (VHT), our effort in parallelizing streaming decision tree induction for distributed environment. Section~\ref{sec:paralellism_type} reviews the available types of parallelism and section~\ref{sec:proposed_algorithm} explains our proposed algorithm.

\section{Parallelism Type}
\label{sec:paralellism_type}
In this thesis, parallelism type refers to the way an algorithm performs parallelization in streaming decision tree induction. Understanding the available parallelism types is important since it is the basis of our proposed distributed algorithm. This section presents three types of parallelism.

For section~\ref{sec:horizontal_parallelism} to ~\ref{sec:task_parallelism}, we need to define some terminologies in order to make the explanation concise and clear. A \textit{processing item}(PI) is an unit of computation element that executes some part of the algorithm. One computation element can be a node, a thread or a process depending on the underlying distributed streaming computation platform. An \textit{user} is a client who executes the algorithm. An user could be a human being or a software component that executes the algorithm such as a machine learning framework. We define an \textit{instance} as a datum in the training data. Since the context is streaming decision tree induction, the training data consists of a set of instances that arrive one at a time to the algorithm. 

\subsection{Horizontal Parallelism}
\label{sec:horizontal_parallelism}

\begin{figure}
	\centering
	\includegraphics[scale=0.36]{figs/horizontal_parallelism.pdf}
	\caption{Horizontal Parallelism}
	\label{fig:horizontal_parallelism}
\end{figure}

Figure~\ref{fig:horizontal_parallelism} shows the implementation of horizontal parallelism for distributed streaming decision tree induction. Source processing item sends instances into the distributed algorithm. This component may comprise of an instance generator, or it simply forwards object from external source such as Twitter firehose. A model-aggregator PI consists of the trained model which is a global decision tree in our case. A local-statistic processing PI contains local decision tree. The distributed algorithm only trains the local decision trees with subset of all instances that arrive into the local-statistic PIs. The algorithm periodically aggregates local statistics (the statistics in local decision tree) into the statistics in global decision tree. User determines the interval period by setting a parameter in the distributed algorithm. After the aggregation finishes, the algorithm needs to update each decision tree in local-statistics PIs.

In horizontal parallelism, the algorithm distributes the arriving instances to local-statistic PIs based on horizontal data partitioning, which means it partitions the arriving instances equally among the number of local-statistic PI. For example, if there are 100 arriving instances and there are 5 local-statistics PIs, then each local-statistic PI receives 20 instances. One way to achieve this is to distribute the arriving instances in round-robin manner. An user determines the parallelism level of the algorithm by setting the number of local-statistic PI to process the arriving instances. If arrival rate of the instances exceeds the total processing rate of the local-statistic PIs, then user should increase the parallelism level.

Horizontal parallelism has several advantages. It is appropriate for scenarios with very high arrival rates. The algorithm also observes the parallelism immediately. User is able to easily add more processing power by adding more PI to cope with arriving instances. However, horizontal parallelism needs high amount of available memory since the algorithm replicates the model in the local-statistic PIs. It is also not suitable for cases where the arriving instance has high number of attributes since the algorithm spends most of its time to calculate the information gain for each attribute. The algorithm introduces additional complexity in propagating the updates from global model into local model in order to keep the model consistency between each local-statistic PI and model-aggregator PI.

%\begin{figure}
%	\centering
%	\includegraphics[scale=0.45]{figs/extended_horizontal_parallelism.pdf}
%	\caption{Extended Horizontal Parallelism}
%	\label{fig:extended_horizontal_parallelism}
%\end{figure}

%If model-aggregator PI is the bottleneck in the actual implementation, we can extend the original implementation into figure~\ref{fig:extended_horizontal_parallelism}. However, this extension introduces extra complexity in synchronizing the decision tree model between more than one model-aggregator PIs.

\subsection{Vertical Parallelism}
\label{sec:vertical_parallelism}

\begin{figure}
	\centering
	\includegraphics[scale=0.4]{figs/vertical_parallelism.pdf}
	\caption{Vertical Parallelism}
	\label{fig:vertical_parallelism}
\end{figure}

Figure~\ref{fig:vertical_parallelism} shows the implementation of vertical parallelism for distributed streaming decision tree induction. Source processing item serves the same purpose as the one in horizontal parallelism (section~\ref{sec:horizontal_parallelism}). However, model-aggregator PI and local-statistic PI have different roles compared to the ones in section~\ref{sec:horizontal_parallelism}. 

In vertical parallelism, local-statistic PIs do not have the local model as in horizontal parallelism. Each local-statistic PI only stores the sufficient statistic of several attributes that are assigned to it and computes the information-theoretic criteria (such as the information gain and gain ratio) based on the assigned statistic. Model aggregator PI consists of a global model, but it distributes the instances by their attributes. For example if each instance has 100 attributes and there are 5 local-statistic PIs, then each local-statistic PI receives 20 attributes for each instance. An user determines the parallelism level of the algorithm by setting the number of local-statistic PI to process the arriving instances. However, increasing the parallelism level may not necessarily improve the performance since the cost of splitting and distributing the instance may exceed the benefit. 

Vertical parallelism is suitable when arriving instances have high number of attributes. The reason is that vertical parallelism spends most of its time in calculating the information gain for each attribute. This type of instance is commonly found in text mining. Most text mining algorithms use a dictionary consists of 10000 to 50000 entries. The text mining algorithms then transform text into instances with the number of attributes equals to the number of the entries in the dictionary. Each word in the text correspond to a boolean attribute in the instances. 

Another case where vertical parallelism suits is when the instances are in the form of \textit{documents}. A document is almost similar to a row or a record in relational database system, but it is less rigid compared to row or record. A document is not required to comply with database schemas such as primary key and foreign key. Concrete example of a document is a tweet where each word in a tweet corresponds to one or more entries in a document. And similar to text mining, documents have a characteristic of high number attributes since practically documents are often implemented as dictionaries which have 10000 to 50000 entries. Since each entry corresponds to an attribute, the algorithm needs to process attributes in the magnitude of ten thousands.

One advantage of vertical parallelism is the algorithm implementation uses lesser total memory compared to horizontal parallelism since it does not replicate the model into local-statistics PI. However, vertical parallelism is not suitable when the number of attributes in the attributes is not high enough so that the cost of splitting and distributing is higher than the benefit obtained by the parallelism.

\subsection{Task Parallelism}
\label{sec:task_parallelism}

\begin{figure}
	\centering
	\includegraphics[scale=0.4]{figs/task_parallelism.pdf}
	\caption{Task Parallelism}
	\label{fig:task_parallelism}
\end{figure}

Figure~\ref{fig:task_parallelism} shows the task parallelism implementation for distributed streaming decision tree induction. We define a \textit{task} as a specific portion of the algorithm. In streaming decision tree induction, the task consists of: sort the arriving instance into correct leaf, update sufficient statistic, and attempt to split the node. 

\begin{figure}
	\centering
	\includegraphics[scale=0.5]{figs/sorter_pi.pdf}
	\caption{Sorter PI in terms of Decision Tree Model}
	\label{fig:sorter_pi_tree_model}
\end{figure}

Task parallelism consists of sorter processing item and updater-splitter processing item. This parallelism distributes the model into the available processing items. Sorter PI consists of part of the decision tree which is a subtree and connection to another subtree as shown in figure~\ref{fig:sorter_pi_tree_model}. It sorts the arriving instance into correct leaf. If the leaf does not exist in the part that the sorter PI owns, the sorter PI forwards the instance into the correct sorter or the updater-splitter PI that contains the path to appropriate leaf. Updater-splitter PI consists of a subtree that has leaves. This PI updates sufficient statistic for the leaf and splits the leaf when the leaf satisfies splitting condition. 

\begin{figure}
	\centering
	\includegraphics[scale=0.65]{figs/task_parallelism_steps.pdf}
	\caption{Decision Tree Induction based on Task Parallelism}
	\label{fig:task_parallelism_steps}
\end{figure}

Figure~\ref{fig:task_parallelism_steps} shows the induction based on task parallelism. This induction process starts with one updater-splitter PI in step (i). This PI grows the tree until the tree reaches memory limit. When this happens, the PI converts itself into sorter PI and it creates new updater-splitter PIs to represent subtrees generated from its leaves, as shown in step (ii) and (iii). The algorithm repeats the process until it uses all available processing items. User configures the number of PIs available for the algorithm. 

Task parallelism is suitable when the model size is very high and the resulting model could not fit in the available memory. However, the algorithm does not observe the parallelism immediately. Only after the algorithm distributes the model, it can observe the parallelism. 

\section{Proposed Algorithm}
\label{sec:proposed_algorithm}
%%%%%%%%%%%%%%%%%%%%%
This section discusses our proposed algorithm for implementing distributed and parallel streaming decision tree induction. The algorithm extends VFDT algorithm presented in section~\ref{sec:stream_dec_tree_induction} with capabilities of performing streaming decision tree induction in distributed and parallel manner.

\subsection{Chosen Parallelism Type}
\label{sec:chosen_parallelism_type}
In order to choose which parallelism type, we revisit the use-case for the proposed algorithm. We derive the use-case by examining the need of Web-mining research group in Yahoo! Labs Barcelona, where we perform this thesis. 

The use-case for our distributed streaming tree induction algorithm is to perform document-streaming and text-mining classification. As section~\ref{sec:vertical_parallelism} describes, both cases involve instances with high number of attributes. Given this use case, vertical parallelism appears to be the suitable choice for our implementation. 

\subsection{Vertical Hoeffding Tree}
\label{sec:vertical_hoeffding_tree}
In this section, we refer our proposed algorithm as \textit{Vertical Hoeffding Tree}(VHT). We reuse some definitions from section~\ref{sec:paralellism_type} for \textit{processing item}(PI), \textit{instance} and \textit{user}. We define additional terms to make the explanation concise and clear. A \textit{stream} is a connection between processing items that transports messages from a PI to the corresponding destination PIs. A \textit{content event} represents a message transmitted by a PI via one or more streams. 

\begin{figure}
	\centering
	\includegraphics[scale=0.5]{figs/VHT_blackwhite.pdf}
	\caption{Vertical Hoeffding Tree}
	\label{fig:vht}
\end{figure}

Figure~\ref{fig:vht} shows the VHT diagram. Each circle represents a processing item. The number inside the circle represents the parallelism level. A model-aggregator PI consists of the decision tree model. It connects to local-statistic PI via \verb;attribute; stream and \verb;control; stream. As we described in section~\ref{sec:vertical_parallelism} about vertical parallelism, the model-aggregator PI splits instances based on attribute and each local-statistic PI contains local statistic for attributes that assigned to it. Model-aggregator PI sends the split instances via \verb;attribute; stream and it sends control messages to ask local-statistic PI to perform computation via \verb;control; stream. Users configure $n$, which is the parallelism level of the algorithm. The parallelism level is translated into the number of local-statistic PIs in the algorithm. 

Model-aggregator PI sends the classification result via \verb;result; stream to an evaluator PI for classifier or other destination PI. Evaluator PI performs evaluation of the algorithm and the evaluation could be in term of accuracy and throughput. Incoming instances to the model-aggregator PI arrive via \verb;source; stream. The calculation results from local statistic arrive to the model-aggregator PI via \verb;computation-result; stream. 

\begin{algorithm}
\caption{model-aggregator PI: VerticalHoeffdingTreeInduction($E$, $VHT\_tree$)}
\begin{algorithmic}[1]
\REQUIRE $E$ is a training instance from source PI, wrapped in \verb;instance; content event
\REQUIRE $VHT\_tree$ is the current state of the decision tree in model-aggregator PI
\STATE Use $VHT\_tree$ to sort $E$ into a leaf $l$s
\STATE Send \verb;attribute; content events to local-statistic PIs
\STATE Increment the number of instances seen at $l$ (which is $n_l$)
\IF{$n_l$ $mod$ $n_{min}$ $= 0$ {\bf and} not all instances seen at $l$ belong to the same class}
\STATE Add $l$ into the list of splitting leaves
\STATE Send \verb;compute; content event with the id of leaf $l$ to all local-statistic PIs
\ENDIF
\end{algorithmic}
\label{alg:vht_tree_induction}
\end{algorithm}

Algorithm ~\ref{alg:vht_tree_induction} shows the pseudocode of the model-aggregator PI in learning phase. The model-aggregator PI has similar steps as learning in VFDT except on the line 2 and 5. Model-aggregator PI receives \verb;instance; content events from source PI and it extracts the instances from the content events. Then, model-aggregator PI needs to split the instances based on the attribute and send \verb;attribute; content event via \verb;attribute; stream to update the sufficient statistic for the corresponding leaf (line 2). \verb;Attribute; content event consists of leaf ID, attribute ID, attribute value, class value, and instance weight. Leaf ID and attribute ID are used by the algorithm to route the content event into correct local-statistic PIs. And attribute value, class value and instance weight are stored as local statistic in local-statistic PIs. 

When a local-statistic PI receives \verb;attribute; content event, it updates its corresponding local statistic. To perform this functionality, it keeps a data structure that store local statistic based on leaf ID and attribute ID. The local statistic here is the attribute value, weight, and the class value. Algorithm~\ref{alg:update_local_statistic} shows this functionality.

\begin{algorithm}
\caption{local-statistic PI: UpdateLocalStatistic($attribute$, $local\_statistic$)}
\begin{algorithmic}[1]
\REQUIRE $attribute$ is an \verb;attribute; content event
\REQUIRE $local\_statistic$ is the local statistic, could be implemented as $Table<leaf\_id, attribute\_id>$
\STATE Update $local\_statistic$ with data in $attribute$: attribute value, class value and instance weights
\end{algorithmic}
\label{alg:update_local_statistic}
\end{algorithm}

When it is the time to grow the tree(algorithm ~\ref{alg:vht_tree_induction} line 4), model-aggregator PI sends \verb;compute; content event via \verb;control; stream to local-statistic PI. Upon receiving \verb;compute; content event, each local-statistic PI calculates $\overline{G}_{l}(X_{i})$ for its assigned attributes to determines the best and second best attributes. At this point,the model-aggregator PI may choose to continue processing incoming testing instance or to wait until it receives all the computation results from local-statistic PI.

Upon receiving \verb;compute; content event, local-statistic PI calculates $\overline{G}_{l}(X_{i})$ for each attribute to find the best and the second best attributes. Then it sends the best($X^{local}_{a}$) and second best($X^{local}_{b}$) attributes back to the model-aggregator PI via \verb;computation-result; stream. These attributes are contained in \verb;local-result; content event. Algorithm~\ref{alg:receive_compute_message} shows this functionality.

\begin{algorithm}
\caption{local-statistic PI: ReceiveComputeMessage($compute$, $local\_statistic$)}
\begin{algorithmic}[1]
\REQUIRE $compute$ is an $compute$ content event
\REQUIRE $local\_statistic$ is the local statistic, could be implemented as $Table<leaf\_id, attribute\_id>$
\STATE Get leaf $l$ ID from $compute$ content event
\STATE For each attribute belongs to leaf $l$ in local statistic, compute $\overline{G}_{l}(X_{i})$
\STATE Find $X^{local}_{a}$, which is the attribute with highest $\overline{G}_{l}$ based on the local statistic
\STATE Find $X^{local}_{b}$, which is the attribute with second highest $\overline{G}_{l}$ based on the local statistic
\STATE Send $X^{local}_{a}$ and $X^{local}_{b}$ using \verb;local-result; content event to model-aggregator PI via \verb;computation-result; stream
\end{algorithmic}
\label{alg:receive_compute_message}
\end{algorithm}

The next part of the algorithm is to update the model once it receives all computation results from local statistic. This functionality is performed in model-aggregator PI. Algorithm~\ref{alg:receive_local_result} shows the pseudocode for this functionality. Whenever the algorithm receives a \verb;local-result; content event, it retrieves the correct leaf $l$ from the list of the splitting leaves(line 1). Then, it updates the current best attribute($X_{a}$) and second best attribute($X_{b}$). If all local results have arrived into model-aggregator PI, the algorithm computes Hoeffding bound and decides whether to split the leaf $l$ or not. It proceeds to split the node if the conditions in line 5 are satisfied. These steps in line 4 to 9 are identical to basic streaming decision tree induction presented in section~\ref{sec:stream_dec_tree_induction}. To handle stragglers, model-aggregator PI has time-out mechanism in waiting for all computation results. If the time out occurs, the algorithm uses the current $X_{a}$ and $X_{b}$ to compute Hoeffding bound and make splitting decision. 

\begin{algorithm}
\caption{model-aggregator PI: Receive($local\_result$, $VHT\_tree$)}
\begin{algorithmic}[1]
\REQUIRE $local\_result$ is an \verb;local-result; content event
\REQUIRE $VHT\_tree$ is the current state of the decision tree in model-aggregator PI
\STATE Get correct leaf $l$ from the list of splitting leaves
\STATE Update $X_{a}$ and $X_{b}$ in the splitting leaf $l$ with $X^{local}_{a}$ and $X^{local}_{b}$ from $local\_result$
\IF{$local\_results$ from all local-statistic PIs received or time out reached}
\STATE Compute Hoeffding bound $\epsilon = \sqrt{\frac{R^{2}\ln(1/\delta)}{2n_{l}}}$
\IF{$X_{a} \ne X_{\emptyset}$ {\bf and} ($\overline{G}_{l}(X_{a}) - \overline{G}_{l}(X_{b}) > \epsilon$ {\bf or} $\epsilon < \tau$)}
\STATE Replace $l$ with a split-node on $X_{a}$
\FORALL{branches of the split}
\STATE Add a new leaf with derived sufficient statistic from the split node
\ENDFOR
\ENDIF
\ENDIF
\end{algorithmic}
\label{alg:receive_local_result}
\end{algorithm}

\newpage
During testing phase, the model-aggregator PI predicts the class value of the incoming instances. Algorithm~\ref{alg:predict_class_value} shows the pseudocode for this functionality. Model aggregator PI uses the decision tree model to sort the newly incoming instance into the correct leaf and use the leaf to predict the class. Then, it sends the class prediction into the \verb;result; stream. 

\begin{algorithm}
\caption{model-aggregator PI: PredictClassValue($test\_instance$, $VHT\_tree$)}
\begin{algorithmic}[1]
\REQUIRE $test\_instance$ is a newly arriving instance
\REQUIRE $VHT\_tree$ is the decision tree model
\STATE Use $VHT\_tree$ to sort $test_instance$ into the correct leaf $l$
\STATE Use leaf $l$ to predict the class of $test_instance$
\STATE Send classification result via \verb;result; stream
\end{algorithmic}
\label{alg:predict_class_value}
\end{algorithm}